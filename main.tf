provider "vsphere" {
  user           = var.vsphere_user
  password       = var.vsphere_password
  vsphere_server = "vcenter.lab.nwa.evtcorp.com"
  allow_unverified_ssl = true
}

resource "vsphere_virtual_disk" "myDisk" {
  size       = 2
  vmdk_path  = "test.vmdk"
  datastore  = "local"
}
