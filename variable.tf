variable "vsphere_user" {
  type        = string
  description = "user for vsphere"
}

variable "vsphere_password" {
  type        = string
  description = "password for vsphere"
}